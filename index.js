const ESDoc = require("esdoc"),
  publisher = require("esdoc/out/src/Publisher/publish")

/**
 * @return Nothing in specific
 */
module.exports = function () {
  this.filter("esdoc", (data, config) => {
    if (config === undefined) {
      config = {}
    }
    return ESDoc.generate(config, publisher)
  })
}
